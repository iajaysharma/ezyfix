@extends('vendor.layouts.app')
@section('content')
<div class="row">
	<div class="col-md-6 col-lg-4"> 
		<div class="card card-hover">
			<div class="box bg-cyan text-center">
				<h1 class="font-light text-white">{{number_format($total_giftcards,0)}}</h1>
				<h6 class="text-white">Gift Cards</h6>
			</div>
		</div> 
	</div>
	<div class="col-md-6 col-lg-4"> 
		<div class="card card-hover">
			<div class="box bg-cyan text-center">
				<h1 class="font-light text-white">{{number_format($total_images,0)}}</h1>
				<h6 class="text-white">Gallery Images</h6>
			</div>
		</div> 
	</div> 
</div>
@endsection