@extends('vendor.layouts.app')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">{{$title}}</h4>
				{{Form::model($data,['id'=>'submit-form','files'=>true])}}
				<div class="form-group m-t-20">
					<label class="">Name</label>
					{{Form::text('name',null,['class'=>'form-control name','placeholder'=>'Name','id'=>'name'])}}
				</div>
				<div class="form-group">
					<label class="">Email</label>
					{{Form::email('email',null,['class'=>'form-control name','placeholder'=>'Email','id'=>'email','readonly'=>true])}}
				</div>
				<div class="form-group">
					<label class="">Logo</label>
					<input type="file" id="logo" data-default-file="{{($data->logo && file_exists($data->logo))?url($data->logo):''}}" name="logo" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif" data-max-file-size="3M" data-allowed-formats="square landscape"/> 
				</div>
				<div class="form-group">
                    <label>Phone*</label>
                    {{Form::number('phone',null,array('placeholder'=>'Phone','id'=>'phone','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Address*</label>
                    {{Form::text('address',null,array('placeholder'=>'Location Name','id'=>'address','class'=>'form-control'))}} 
                    {{Form::hidden('latitude',null,array('placeholder'=>'Location Name','id'=>'latitude','class'=>'form-control','id'=>'latitude'))}}
                    {{Form::hidden('longitude',null,array('placeholder'=>'Location Name','id'=>'longitude','class'=>'form-control','id'=>'longitude'))}}
                    <span class="text-danger error address latitude longitude"></span>
                </div>   
                <div class="form-group">
                    <label>Description</label>
                    {{Form::textarea('description',null,array('placeholder'=>'Enter Description','id'=>'description','class'=>'form-control','rows'=>5))}}
                </div> 
				<button type="submit" id="submit-btn" class="btn btn-secondary btn-block"><span id="licon"></span>Save</button>
				{{Form::close()}}
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyASPYwXN1wRC-Hg3BNTmuC1veIEgxLQWBE&libraries=places&country=In" type="text/javascript"></script>
<script>
$(function(){
	$('#submit-form').ajaxForm({
		beforeSubmit:function(){
			$(".error").remove();
			disable("#submit-btn",true); 
		},
		error:function(err){ 
			handleError(err);
			disable("#submit-btn",false);  
		},
		success:function(response){ 
			disable("#submit-btn",false); 
			if(response.status=='true'){
				Alert(response.message);
			}else{
				Alert(response.message,false);
			}
		}
    }); 
});
function initialize() {
    var input = document.getElementById('address');
    var options = { 
      componentRestrictions: {country: "in"}
     };
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        document.getElementById('latitude').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
        //alert("This function is working!");
        //alert(place.name);
       //console.log(place.address_components); 
    });
}
google.maps.event.addDomListener(window, 'load', initialize); 
</script>
@endsection