@extends('vendor.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body"> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dtable" id="users-table">
                        <thead>
                            <th width="30">Id</th>    
                            <th>User</th>     
                            <th>Giftcard</th>     
                            <th>Code</th>     
                            <th>Approval Status</th>    
                            <th>Created</th>   
                        </thead> 
                    </table>
                </div>        
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script> 
    var oTable =  $('#users-table').DataTable({
        processing: true,
        serverSide: true, 
         ajax:{
            url : '{!! route('vendor.orders.datatables') !!}',
            data: function (d) {
               // d.parent = $('#parent').val(); 
            }
        },
        columns: [
            {data: 'id', name: 'id'},   
            {data: 'user.name', name: 'user.name'},  
            {data: 'giftcard.title', name: 'giftcard.title'},  
            {data: 'code', name: 'code'},   
            {data: 'is_vendor_approved', name: 'is_vendor_approved'}, 
            {data: 'created_at', name: 'created_at'}
        ],
        order:[[0,'desc']], 
    });
</script>
@endsection