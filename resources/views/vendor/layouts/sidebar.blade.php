<?php
$currentAction = \Route::currentRouteAction();
list($controller, $action) = explode('@', $currentAction);
$controller = preg_replace('/.*\\\/', '', $controller);
// dd($controller);
?>
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('vendor.home')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                <?php $active = ($controller=="GiftcardController" && in_array($action,['index','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('vendor.giftcards.index')}}" aria-expanded="false"><i class="mdi mdi-wallet-giftcard"></i><span class="hide-menu">Gift Cards</span></a></li>

                <?php $active = ($controller=="GalleryController" && in_array($action,['index','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('vendor.gallery.images')}}" aria-expanded="false"><i class="mdi mdi-folder-multiple-image"></i><span class="hide-menu">Manage Gallery</span></a></li>

                <?php $active = ($controller=="OrderController" && in_array($action,['index']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('vendor.orders.index')}}" aria-expanded="false"><i class="mdi mdi-cash-usd"></i><span class="hide-menu">Orders</span></a></li> 
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
</aside>