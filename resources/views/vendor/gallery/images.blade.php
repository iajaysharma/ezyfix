@extends('vendor.layouts.app')
@section('styles')
<link rel="stylesheet" href="{{url('public/admin/assets/libs/dropzone/dropzone.css')}}">
<style>
    .dropzone{
        width: 100%;
        padding: 50px 20px;
        transition: linear 0.3s;
        border: 5px dashed #dee2e6;
    }
    .dropzone:hover{
        background: #dee2e6; 
    }
    .img-card{
        height: 200px;
        overflow: hidden;
    }
    .img-card img{
        max-height: 200px;
    }
</style>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$title}}</h4>
                <hr> 
                <div class="row col-md-12">
                    <form name="frmImage" action="{{route('vendor.gallery.upload_images')}}" class="dropzone">
                        {{csrf_field()}}
                    </form>
                </div>
                <hr>
                <h4 class="card-title">Images</h4><hr> 
                <div class="row el-element-overlay m-t-20 image-content">

                     <!-- <div class="col-lg-3 col-md-6">
                           <div class="card">
                               <div class="el-card-item">
                                   <div class="el-card-avatar el-overlay-1"> <img src="https://images.pexels.com/photos/1803914/pexels-photo-1803914.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="user">
                                       <div class="el-overlay">
                                           <ul class="list-style-none el-info">
                                               <li class="el-item"><a class="btn default btn-outline el-link" href="javascript:void(0);"><i class="mdi mdi-delete"></i></a></li>
                                           </ul>
                                       </div>
                                   </div>                                
                               </div>
                           </div>
                                            </div> -->   
                </div>    
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{url('public/admin/assets/libs/dropzone/dropzone.js')}}"></script>
<script>
    var url = '{!! url('') !!}'+'/';
    $(function(){
        getImages(); 
    });

    function deleteImage(id){
        if(confirm("You won't be able to revert this!")){
            $.ajax({
                url: '{{route('vendor.gallery.delete')}}',
                type: 'POST',
                data:{id:id},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                  var data = JSON.parse(response)
                  if(data.type=='success'){
                        getImages();
                  }else{
                    alert(data.data);
                  }
                  //$(".preloader").hide();
                },error:function(error){
                  //$(".preloader").hide();  
                  var data = JSON.parse(error)
                  alert(data.data);

                }
            });
        } 
    }
    function getImages(){
        $(".image-content").html('<div class="text-center w-100"><i class="fas fa-circle-notch  fa-spin fa-5x"></i></div>');
        $.ajax({
            url: '{!! route('vendor.gallery.get_images') !!}',
            type: "GET",
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) { 
              var content = '';  
              if(data.status==true){
                data.data.map(image=>{
                    content += '<div class="col-lg-3 col-md-6"><div class="card img-card"><div class="el-card-item"><div class="el-card-avatar el-overlay-1"> <img src="'+url+image.image+'" alt="user"><div class="el-overlay"><ul class="list-style-none el-info"><li class="el-item"><a class="btn default btn-outline el-link" onclick="deleteImage('+image.id+')" href="javascript:void(0);"><i class="mdi mdi-delete"></i></a></li></ul></div></div></div></div></div>';
                });
                $(".image-content").html(content);
              }else{
                $(".image-content").html("<div class='text-center w-100'>"+data.message+"</div>");
              }
            },
            error: function (data) {
                 $(".image-content").html("<div class='text-center w-100'>Error while fetching Images, please try again.</div>");
            }
        });
    } 
</script>
@endsection