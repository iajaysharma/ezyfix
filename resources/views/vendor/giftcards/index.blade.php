@extends('vendor.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="text-right mb-2">
                    <a href="{{route('vendor.giftcards.add')}}" class="btn btn-secondary btn-md">Add New</a>
                </div> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dtable" id="users-table">
                        <thead>
                            <th width="30">Id</th>    
                            <th>Category</th>     
                            <th>Title</th>     
                            <th>Price</th>    
                            <th>Validity Type</th>    
                            <th>Image</th>    
                            <th width="50">Status</th>    
                            <th width="115">Action</th>    
                        </thead> 
                    </table>
                </div>        
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script> 
    var oTable =  $('#users-table').DataTable({
        processing: true,
        serverSide: true, 
         ajax:{
            url : '{!! route('vendor.giftcards.datatables') !!}',
            data: function (d) {
               // d.parent = $('#parent').val(); 
            }
        },
        columns: [
            {data: 'id', name: 'id'},   
            {data: 'category.title', name: 'category.title'},  
            {data: 'title', name: 'title'},  
            {data: 'price', name: 'price'},  
            {data: 'validity_type', name: 'validity_type'},
            {data: 'image', name: 'image'},
            {data: 'status', name: 'status'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']], 
    });
</script>
@endsection