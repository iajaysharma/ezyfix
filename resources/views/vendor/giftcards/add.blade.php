@extends('vendor.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}}  
                <div class="form-group">
                    <label>Category</label>
                    {{Form::select('category_id',$categories,null,array('placeholder'=>'Choose Category','id'=>'category_id','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Display Price</label>
                    {{Form::text('display_price',null,array('placeholder'=>'Display Price','id'=>'display_price','class'=>'form-control only_number'))}}
                </div>
                <div class="form-group">
                    <label>Price</label>
                    {{Form::text('price',null,array('placeholder'=>'Price','id'=>'price','class'=>'form-control only_number'))}}
                </div>
                <div class="form-group">
                    <label>Validity Type</label>
                    {{Form::select('validity_type',['ALL_DAYS'=>"All Days",'MANUAL'=>"Manual"],null,array('id'=>'validity_type','class'=>'form-control'))}}
                </div>
                <div class="row" id="date-outer" {{(!$data || ($data && $data->validity_type=='ALL_DAYS'))?"style=display:none":""}}>
                    <div class="col-6">
                        <div class="form-group">
                            <label>Start Date</label>
                            {{Form::text('start_date',null,array('placeholder'=>'Start Date','id'=>'start_date','class'=>'form-control datepicker'))}}
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>End Date</label>
                            {{Form::text('end_date',null,array('placeholder'=>'End Date','id'=>'end_date','class'=>'form-control datepicker'))}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group bootstrap-timepicker">
                            <label>Start Time</label>
                            {{Form::text('start_time',null,array('placeholder'=>'Start Time','id'=>'start_time','class'=>'form-control timepicker bootstrap-timepicker'))}}
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label>End Time</label>
                            {{Form::text('end_time',null,array('placeholder'=>'End Time','id'=>'end_time','class'=>'form-control timepicker'))}}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Image</label>
                    @if($id)
                    <input type="file" accept="image/*" id="image" data-default-file="{{($data->image && file_exists($data->image))?url($data->image):''}}" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @else
                    <input type="file" accept="image/*" id="image" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @endif
                </div> 
                <div class="form-group">
                    <label>Description</label>
                    {{Form::textarea('description',null,array('placeholder'=>'Enter Description','id'=>'description','class'=>'form-control','rows'=>5))}}
                </div>  
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                <a class="btn btn-secondary" href="{{route('vendor.giftcards.index')}}">Back</a> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection 
@section('styles')
<link rel="stylesheet" href="{{url('public/admin/assets/libs/summernote/summernote-bs4.css')}}">
<link rel="stylesheet" href="{{url('public/admin/dist/css/timepicker.css')}}">
@endsection
@section('scripts')
<script src="{{url('public/admin/assets/libs/summernote/summernote-bs4.min.js')}}"></script>
<script src="{{url('public/admin/dist/js/bootstrap-timepicker.js')}}"></script>
<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('vendor.giftcards.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 

  $('#description').summernote({
      height:200
  });

    $("#validity_type").on('change',function(e){ 
        if(e.target.value=='ALL_DAYS'){
            $("#date-outer").fadeOut(); 
        }else{ 
            $("#date-outer").fadeIn(); 
        }
    })
    $('.datepicker').datepicker({
        format:"dd-mm-yyyy"
    });
    $('#start_time').timepicker({
        defaultTime:"09:00 AM"
    });
    $('#end_time').timepicker({
        defaultTime:"06:00 PM"
    });
});
</script>
@endsection
