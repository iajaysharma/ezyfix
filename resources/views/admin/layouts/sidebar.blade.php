<?php
$currentAction = \Route::currentRouteAction();
list($controller, $action) = explode('@', $currentAction);
$controller = preg_replace('/.*\\\/', '', $controller);
// dd($controller);
?>
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.home')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>

                <?php $active = ($controller=="UserController" && in_array($action,['index','view']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.users.index')}}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Users</span></a></li>

                <?php $active = ($controller=="VendorController" && in_array($action,['index','view','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.vendors.index')}}" aria-expanded="false"><i class="mdi mdi-shopping"></i><span class="hide-menu">Vendors</span></a></li>

                <?php $active = ($controller=="CategoryController" && in_array($action,['index','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.categories.index')}}" aria-expanded="false"><i class="mdi mdi-cube-outline"></i><span class="hide-menu">Categories</span></a></li>
 

                <?php $active = ($controller=="PageController" && in_array($action,['index','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.pages.index')}}" aria-expanded="false"><i class="mdi mdi-page-layout-body"></i><span class="hide-menu">Pages</span></a></li>

                <?php $active = ($controller=="FaqController" && in_array($action,['index','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.faqs.index')}}" aria-expanded="false"><i class="mdi mdi-network-question"></i><span class="hide-menu">Faqs</span></a></li>

                <?php $active = ($controller=="SendNotificationController" && in_array($action,['send','index']) )?"active selected":""; ?>
                <!-- <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.notification.index')}}" aria-expanded="false"><i class="mdi mdi-page-layout-body"></i><span class="hide-menu">Send Notification</span></a></li> -->

                @if(env("APP_DEBUG")=='true')
                <?php $active = ($controller=="EmailTemplateController" && in_array($action,['index','add']) )?"active selected":""; ?>
                <li class="sidebar-item {{$active}}"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin.emailtemplates.index')}}" aria-expanded="false"><i class="mdi mdi-email"></i><span class="hide-menu">Email Templates</span></a></li> 
                @endif

                <!-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-alert"></i><span class="hide-menu">Errors </span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item"><a href="error-403.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 403 </span></a></li>
                    <li class="sidebar-item"><a href="error-404.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 404 </span></a></li>
                    <li class="sidebar-item"><a href="error-405.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 405 </span></a></li>
                    <li class="sidebar-item"><a href="error-500.html" class="sidebar-link"><i class="mdi mdi-alert-octagon"></i><span class="hide-menu"> Error 500 </span></a></li>
                </ul>
            </li>-->   
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
</aside>