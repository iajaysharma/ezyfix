@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="text-right mb-2">
                    <a href="{{route('admin.categories.add')}}" class="btn btn-secondary btn-md">Add New</a>
                </div>
                <!-- <div class="row"> 
                    <div class="col-md-4">
                        <div class="form-group">
                        <label>Filter by Parent Catgory</label> 
                        {{Form::select('parent',$parentCate,null,array('placeholder'=>'All Category','id'=>'parent','class'=>'form-control'))}}
                        </div>
                    </div> 
                </div> -->
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dtable" id="users-table">
                        <thead>
                            <th width="30">Id</th>    
                            <th>Title</th>    
                            <th>Slug</th>    
                            <th width="90">Image</th>    
                            <th width="50">Position</th>    
                            <th width="50">Status</th>    
                            <th width="115">Action</th>    
                        </thead> 
                    </table>
                </div>        
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    
        var oTable =  $('#users-table').DataTable({
        processing: true,
        serverSide: true, 
         ajax:{
            url : '{!! route('admin.categories.datatables') !!}',
            data: function (d) {
               d.parent = $('#parent').val(); 
            }
        },
        columns: [
            {data: 'id', name: 'id'},   
            {data: 'title', name: 'title'},  
            {data: 'slug', name: 'slug'},  
            {data: 'image', name: 'image'},
            {data: 'position', name: 'position'},
            {data: 'status', name: 'status'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']], 
    });

 $('#parent').on('change', function(e) {
        oTable.draw();
        e.preventDefault();
    });

</script>
@endsection