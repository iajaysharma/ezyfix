    @extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}} 
                
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                </div> 
                <div class="form-group">
                    <label>Image</label>
                    @if($id)
                    <input type="file" accept="image/*" id="image" data-default-file="{{($data->image && file_exists($data->image))?url($data->image):''}}" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @else
                    <input type="file" accept="image/*" id="image" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @endif
                </div>
                <!-- <div class="form-group icon">
                    <label>Icon</label>
                    @if($id)
                    <input type="file" accept="image/*" id="icon" data-default-file="{{($data->icon && file_exists($data->icon))?url($data->icon):''}}" name="icon" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @else
                    <input type="file" accept="image/*" id="icon" name="icon" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @endif
                </div> --> 
                <div class="form-group">
                    <label>Content</label>
                    {{Form::textarea('description',null,array('placeholder'=>'Enter Description','id'=>'description','class'=>'form-control','rows'=>5))}}
                </div> 
                <div class="form-group">
                    <label>Position</label>
                    {{Form::number('position',null,array('placeholder'=>'Position','id'=>'position','class'=>'form-control'))}}
                </div> 
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                <a class="btn btn-secondary" href="{{route('admin.categories.index')}}">Back</a> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection 
@section('scripts') 

<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('admin.categories.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 

  $('#description').summernote({
      height:400
  });

  $("#type").on('change',function(e){ 
        if(e.target.value=='PAGE'){
            $(".parent").hide();
            $(".icon").hide();
            $("#parent_id").attr("disabled",true);
        }else{
            $(".parent").show();
            $(".icon").show();
            $("#parent_id").removeAttr("disabled");
        }
  })
});
</script>
@endsection
