    @extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}}  
                <div class="form-group">
                    <label>Username</label>
                    {{Form::text('username',null,array('placeholder'=>'Username','id'=>'username','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Description</label>
                    {{Form::textarea('description',null,array('placeholder'=>'Description','id'=>'description','class'=>'form-control'))}}
                </div> 
                <div class="form-group">
                    <label>Rating</label>
                    {{Form::select('rating',array(1=>1,2=>2,3=>3,4=>4,5=>5),null,['id'=>'rating','class'=>'form-control'])}}
                </div> 
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                <a class="btn btn-secondary" href="{{route('admin.reviews.index')}}">Back</a> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('styles')
<link rel="stylesheet" href="{{url('public/admin/assets/libs/summernote/summernote-bs4.css')}}">
@endsection
@section('scripts')
<script src="{{url('public/admin/assets/libs/summernote/summernote-bs4.min.js')}}"></script>

<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('admin.reviews.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 

  $('#content').summernote({
      height:400
  });

   
});
</script>
@endsection
