@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="text-right mb-2">
                    <a href="{{route('admin.reviews.add')}}" class="btn btn-secondary btn-md">Add New</a>
                </div> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dtable" id="users-table">
                        <thead>
                            <th width="30">Id</th>     
                            <th>Username</th>    
                            <th>Title</th>    
                            <th>Description</th>   
                            <th width="90">Rating</th>    
                            <th width="50">Status</th>    
                            <th width="115">Action</th>    
                        </thead> 
                    </table>
                </div>        
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    
        var oTable =  $('#users-table').DataTable({
        processing: true,
        serverSide: true, 
         ajax:{
            url : '{!! route('admin.reviews.datatables') !!}', 
        },
        columns: [
            {data: 'id', name: 'id'}, 
            {data: 'username', name: 'username'},  
            {data: 'title', name: 'title'},  
            {data: 'description', name: 'description'},  
            {data: 'rating', name: 'rating'}, 
            {data: 'status', name: 'status'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']], 
    }); 
</script>
@endsection