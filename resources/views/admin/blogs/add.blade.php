    @extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}}  
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Title','id'=>'title','class'=>'form-control'))}}
                </div> 
                <div class="form-group">
                    <label>Image</label>
                    @if($id)
                    <input type="file" accept="image/*" id="image" data-default-file="{{($data->image && file_exists($data->image))?url($data->image):''}}" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @else
                    <input type="file" accept="image/*" id="image" name="image" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @endif
                </div> 
                <div class="form-group">
                    <label>Content</label>
                    {{Form::textarea('content',null,array('id'=>'content','class'=>'form-control','rows'=>5))}}
                </div>  
                <div class="form-group">
                    <label>Meta Title</label>
                    {{Form::text('meta_title',null,array('placeholder'=>'Meta Title','id'=>'meta_title','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Meta keywords</label>
                    {{Form::text('meta_keywords',null,array('placeholder'=>'Meta Keywords','id'=>'meta_keywords','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Meta Description</label>
                    {{Form::textarea('meta_description',null,array('placeholder'=>'Meta Description','id'=>'meta_description','class'=>'form-control','rows'=>3))}}
                </div>
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                <a class="btn btn-secondary" href="{{route('admin.blogs.index')}}">Back</a> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('styles')
<link rel="stylesheet" href="{{url('public/admin/assets/libs/summernote/summernote-bs4.css')}}">
@endsection
@section('scripts')
<script src="{{url('public/admin/assets/libs/summernote/summernote-bs4.min.js')}}"></script>

<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('admin.blogs.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 

  $('#content').summernote({
      height:400
  });

   
});
</script>
@endsection
