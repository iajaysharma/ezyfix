@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body"> 
                <div class="col-lg-12">
                    <div class="ui raised segment">
                        <div class="avatar text-center">
                            @if($data->thumb!="" && file_exists($data->thumb))
                            <img src="{{url($data->thumb)}}" alt="" class="img-thumbnail" style="max-height:150px;">
                            @endif
                        </div>
                        <h3 class="text-center" style="margin-top: 5px;">{{$data->name}}</h3>
                        <hr>
                        <div style="text-align:center">
                            <div class="row">
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted">{{$data->name}}</p>
                                </div>
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">{{@($data->email)?$data->email:'---'}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted">
                                        @if($data->mobile)
                                            +{{$data->country_code}}-{{$data->mobile}}
                                        @else
                                            ---
                                        @endif
                                    </p>
                                </div>
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Dob</strong>
                                    <br>
                                    <p class="text-muted">{{@($data->dob)?$data->dob:'---'}}</p>
                                </div>
                            </div>   
                             
                            <div class="row"> 
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Created On</strong>
                                    <br>
                                    <p class="text-muted">{{date("d-M, Y",strtotime($data->created_at))}}</p>
                                </div>
                                <div class="col-md-6 col-xs-6 b-r m-b-15"> <strong>Status</strong>
                                    <br>
                                    @if($data->status==1)
                                    <span class="text-success btn-sm">Active</span>
                                    @else
                                    <span class="text-danger btn-sm">Inactive</span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-success" href="javascript:void(0)" onclick="goBack()">Go Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection