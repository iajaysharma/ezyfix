@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{$title}}</h4>
                <hr>
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}}
                <div class="form-group">
                    <label>Android Version</label>
                    {{Form::text('android_version',null,array('placeholder'=>'Android App version','id'=>'android_version','class'=>'form-control'))}}
                </div>

                <div class="form-group">
                    <label>Ios Version</label>
                    {{Form::text('ios_version',null,array('placeholder'=>'Ios App version','id'=>'ios_version','class'=>'form-control'))}}
                </div> 
                <div class="form-group">
                    <label>Android Force Update</label> 
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="android_force_update" {{($data->android_force_update == 1)?'checked':''}} class="custom-control-input" id="android_force_update" value="1">
                        <label class="custom-control-label" for="android_force_update">Yes</label>
                    </div>
                     <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="android_force_update" class="custom-control-input" id="android_force_update1" value="0" {{($data->android_force_update == 0)?'checked':''}}>
                        <label class="custom-control-label" for="android_force_update1">No</label>
                    </div>
                </div>

                <div class="form-group">
                    <label>Ios Force Update</label> 
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="ios_force_update" {{($data->ios_force_update == 1)?'checked':''}} class="custom-control-input" id="ios_force_update" value="1">
                        <label class="custom-control-label" for="ios_force_update">Yes</label>
                    </div>
                     <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="ios_force_update" class="custom-control-input" id="ios_force_update1" value="0" {{($data->ios_force_update == 0)?'checked':''}}>
                        <label class="custom-control-label" for="ios_force_update1">No</label>
                    </div>
                </div>

                <div class="form-group">
                    <label>Android Maintenance</label>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="android_maintenance" class="custom-control-input" {{($data->android_maintenance == 1)?'checked':''}} id="android_maintenance1" value="1">
                        <label class="custom-control-label" for="android_maintenance1">Yes</label>
                    </div>
                     <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="android_maintenance" class="custom-control-input" id="android_maintenance"  value="0" {{($data->android_maintenance == 0)?'checked':''}}>
                        <label class="custom-control-label" for="android_maintenance">No</label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Ios Maintenance</label>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="ios_maintenance" class="custom-control-input" {{($data->ios_maintenance == 1)?'checked':''}} id="ios_maintenance1" value="1">
                        <label class="custom-control-label" for="ios_maintenance1">Yes</label>
                    </div>
                     <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="radio" name="ios_maintenance" class="custom-control-input" id="ios_maintenance"  value="0" {{($data->ios_maintenance == 0)?'checked':''}}>
                        <label class="custom-control-label" for="ios_maintenance">No</label>
                    </div>
                </div>  
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection
 

@section('scripts') 
<script>
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        Alert(response.message,true);
      }else{
        Alert(response.message,false);
      }
    }
  });
</script>
@endsection