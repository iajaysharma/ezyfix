    @extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">  
                {{Form::model($data,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}} 
                <div class="form-group">
                    <label>Name*</label>
                    {{Form::text('name',null,array('placeholder'=>'Name','id'=>'name','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Email*</label>
                    {{Form::email('email',null,array('placeholder'=>'Email','id'=>'email','class'=>'form-control'))}}
                </div>
                @if(!$id)
                <div class="form-group">
                    <label>Password*</label>
                    {{Form::password('password',array('placeholder'=>'Password','id'=>'password','class'=>'form-control'))}}
                </div>
                @endif
                <div class="form-group">
                    <label>Phone*</label>
                    {{Form::number('phone',null,array('placeholder'=>'Phone','id'=>'phone','class'=>'form-control'))}}
                </div>
                <div class="form-group">
                    <label>Address*</label>
                    {{Form::text('address',null,array('placeholder'=>'Location Name','id'=>'address','class'=>'form-control'))}} 
                    {{Form::hidden('latitude',null,array('placeholder'=>'Location Name','id'=>'latitude','class'=>'form-control','id'=>'latitude'))}}
                    {{Form::hidden('longitude',null,array('placeholder'=>'Location Name','id'=>'longitude','class'=>'form-control','id'=>'longitude'))}}
                    <span class="text-danger error address latitude longitude"></span>
                </div> 
                <div class="form-group logo">
                    <label>Logo</label>
                    @if($id)
                    <input type="file" accept="image/*" id="logo" data-default-file="{{($data->logo && file_exists($data->logo))?url($data->logo):''}}" name="logo" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @else
                    <input type="file" accept="image/*" id="logo" name="logo" class="dropify" data-height="150" data-show-remove="false" data-allowed-file-extensions="png jpeg jpg gif bmp webp svg tif ico"/>
                    @endif
                </div> 
                <div class="form-group">
                    <label>Description</label>
                    {{Form::textarea('description',null,array('placeholder'=>'Enter Description','id'=>'description','class'=>'form-control','rows'=>5))}}
                </div>  
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                <a class="btn btn-secondary" href="{{route('admin.vendors.index')}}">Back</a> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection 
@section('scripts') 
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyASPYwXN1wRC-Hg3BNTmuC1veIEgxLQWBE&libraries=places&country=In" type="text/javascript"></script>
<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('admin.vendors.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 
});

function initialize() {
    var input = document.getElementById('address');
    var options = { 
      componentRestrictions: {country: "in"}
     };
    var autocomplete = new google.maps.places.Autocomplete(input,options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        document.getElementById('latitude').value = place.geometry.location.lat();
        document.getElementById('longitude').value = place.geometry.location.lng();
        //alert("This function is working!");
        //alert(place.name);
       //console.log(place.address_components); 
    });
}
google.maps.event.addDomListener(window, 'load', initialize); 
</script>
@endsection
