@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="text-right mb-2">
                    <a href="{{route('admin.vendors.add')}}" class="btn btn-secondary btn-md">Add New</a>
                </div> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dtable" id="users-table">
                        <thead>
                            <th width="30">Id</th>    
                            <th>Name</th>    
                            <th>Email</th>    
                            <th width="90">Phone</th>    
                            <th>address</th>      
                            <th width="50">Status</th>    
                            <th width="115">Action</th>    
                        </thead> 
                    </table>
                </div>        
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script> 
        var oTable =  $('#users-table').DataTable({
        processing: true,
        serverSide: true, 
         ajax:{
            url : '{!! route('admin.vendors.datatables') !!}',
            data: function (d) {
               // d.parent = $('#parent').val(); 
            }
        },
        columns: [
            {data: 'id', name: 'id'},   
            {data: 'name', name: 'name'},  
            {data: 'email', name: 'email'},  
            {data: 'phone', name: 'phone'},
            {data: 'address', name: 'address'},
            {data: 'status', name: 'status'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        order:[[0,'desc']], 
    });

    $('#parent').on('change', function(e) {
        oTable.draw();
        e.preventDefault();
    });

</script>
@endsection