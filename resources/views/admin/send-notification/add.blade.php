@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                {{Form::model(null,array('files'=>'true','class'=>'','id'=>'submit-form','autocomplete'=>'off'))}}
                 <div class="form-group">
                    <label>User Type</label>
                    {{Form::select('user_type',array('Both'=>'Both','CPA'=>'CPA','USER'=>'USER'),null,array('id'=>'user_type','class'=>'form-control'))}}
                    <!-- <span class="text-danger error user_type">{{$errors->first('user_type')}}</span> -->
                </div>
                <div class="form-group">
                    <label>Title</label>
                    {{Form::text('title',null,array('placeholder'=>'Notification Title','id'=>'title','class'=>'form-control'))}}
                </div> 
                <div class="form-group">
                    <label>Content</label>
                    {{Form::textarea('content',null,array('placeholder'=>'Notification Content','id'=>'content','class'=>'form-control content'))}}
                </div>  
                <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Send</button> 
                <a class="btn btn-secondary" href="{{route('admin.notification.index')}}">Back</a> 
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('styles')
<link rel="stylesheet" href="{{url('public/admin/assets/libs/summernote/summernote-bs4.css')}}">
@endsection
@section('scripts')
<script src="{{url('public/admin/assets/libs/summernote/summernote-bs4.min.js')}}"></script>
<script>
$(function(){
  $('#submit-form').ajaxForm({
    beforeSubmit:function(){
      $(".error").remove();
      disable("#submit-btn",true); 
    },
    error:function(err){ 
      handleError(err);
      disable("#submit-btn",false);  
    },
    success:function(response){ 
      disable("#submit-btn",false); 
      if(response.status=='true'){
        window.location.href = '{{route('admin.notification.index')}}';
      }else{
        Alert(response.message,false);
      }
    }
  }); 

/*  $('.content').summernote({
      height:300
  });*/
});


</script>
@endsection