@extends('admin.layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="text-right mb-3">
                    <a href="{{route("admin.send.notification")}}" class="btn btn-secondary">Send Notification</a>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered dtable" id="users-table">
                        <thead>
                            <th>id</th>    
                            <th>Title</th>    
                            <th>Message</th>    
                            <th>Created On</th>    
                            <th>Action</th>    
                        </thead> 
                    </table>
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
     $('#users-table').DataTable({
        processing: true,
        serverSide: true, 
        ajax: '{!! route('admin.notification.datatables') !!}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'message', name: 'message'},
            {data: 'created_at', name: 'created_at'}, 
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
         order:[[0,'desc']],
    });
</script>
@endsection