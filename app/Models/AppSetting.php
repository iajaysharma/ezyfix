<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    //
 
    protected $fillable = [
        'android_version','ios_version','android_force_update','ios_force_update','android_maintenance','ios_maintenance'
    ];
}
