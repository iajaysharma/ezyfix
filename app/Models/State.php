<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    protected $fillable = [
        'name', 'shortcode'
    ];
    public $timestamps = false;
     
}
