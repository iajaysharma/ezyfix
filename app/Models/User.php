<?php

namespace App\Models;

//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','profile_picture','country_code','mobile','token','status','notification','dob','state'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getProfile($id){
        return User::where('id','=',$id)->first();
    }

    public function country(){
       return $this->belongsTo('App\Models\Country','country_id');
    } 

   

    public function gallery(){
        return $this->hasMany(CpaGallery::class,'user_id');
    }

    public function documents(){
        return $this->hasMany(CpaDocument::class,'user_id');
    } 

    public function efillings(){
        return $this->hasMany(CpaEfilling::class,'user_id');
    }
    public function educations(){
        return $this->hasMany(CpaEducation::class,'user_id');
    }

    public function spouce(){
        return $this->hasOne(SpouceDetail::class);
    }

    public function kids(){
        return $this->hasMany(UserKid::class);
    }

    public function statename(){
        return $this->belongsTo(State::class,'state');
    }

    public function occupation(){
        return $this->belongsTo(Occupation::class,'occupation');
    }

    public function language(){
        return $this->belongsTo(Language::class,'language','shortcode');
    }
     
    public function cpaSpecializationPerformance(){
        return $this->hasMany(CpaSpecializationPerformance::class);
    }
}
