<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
class UserAddress extends Model
{  
	use SoftDeletes;   

    protected $fillable = [
        'is_default','user_id','city','zip_code','address_type','sublocality','lat','lng','country','address_type_desc','address'
    ]; 

}
