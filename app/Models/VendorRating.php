<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
class VendorRating extends Model
{  
	use SoftDeletes;   

    protected $fillable = [
        'user_id','vendor_id','rating','message'
    ];  

    public function user(){
        return $this->belongsTo(User::class);
    }

}
