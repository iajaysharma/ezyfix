<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{  
    protected $fillable = [
        'user_id','notification_type','title','message','data','is_seen','language'
    ]; 

    public function eventby(){
       return $this->hasOne('App\Models\User','id','other_user_id');
    }
}
