<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model; 

class VendorImage extends Model
{   

    protected $fillable = [
        'uuid','vendor_id','image'
    ]; 

}
