<?php

namespace App\Models; 

use Illuminate\Foundation\Auth\User as Authenticatable; 

class Vendor extends Authenticatable
{ 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guard = 'vendor';

    protected $fillable = [
        'name', 'email', 'password','description','address','latitude','longitude','phone','logo','status','remember_token','sidebar_style'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function giftcards(){
        return $this->hasMany(GiftCard::class);
    }

    public function images(){
        return $this->hasMany(VendorImage::class);
    }

}
