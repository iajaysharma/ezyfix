<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Str;
class Order extends Model
{  
	use SoftDeletes;

    protected $fillable = [
        'uuid','user_id','giftcard_id','vendor_id','code','is_vendor_approved'
    ];  

    protected static function booted()
    {
        static::creating(function ($order) {
            $order->uuid =  Str::orderedUuid();
        });
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function giftcard(){
        return $this->belongsTo(GiftCard::class,'giftcard_id');
    }

}
