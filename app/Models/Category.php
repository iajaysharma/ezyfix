<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
class Category extends Model
{  
	use SoftDeletes,Sluggable;   

    protected $fillable = [
        'title','slug','image','status','description','parent_id','position','subtitle','icon','type','meta_title','meta_keywords','meta_description'
    ]; 

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function categories()
    {
        return $this->hasMany(Category::class,'parent_id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class,'parent_id')->with('childrenCategories');
    }

}
