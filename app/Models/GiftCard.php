<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
class GiftCard extends Model
{  
	use SoftDeletes;   

    protected $fillable = [
        'uuid','vendor_id','title','display_price','price','validity_type','start_date','end_date','start_time','end_time','description','image','status','formated_start_time','formated_end_time','category_id'
    ];  

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function order(){
        return $this->hasOne(Order::class,'giftcard_id');
    }

}
