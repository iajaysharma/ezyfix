<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;
use App\Models\Country;
use App\Models\UserDevices;
use App\Models\User; 
use App\Models\Page; 
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Otp; 
use App\Models\AppSetting;
use App\Models\Category;
use App\Models\Vendor;
use App\Models\VendorRating;
use App\Models\GiftCard;
use App\Models\UserAddress;
use App\Lib\Uploader;
use App\Lib\Email;
use App\Lib\Helper;
use App\Lib\Twilio; 
use App\Models\Order; 
use DB;
use Hash;
use Validator;
use Str; 
use Image;  
use Log;  
class UserController extends Controller
{
    
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
           
    } 

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */ 

    // This method use for signup
    protected function signup(Request $request){
        try { 
            $data = $request->all(); 
            $validator = Validator::make($data, [
                'name'              => 'required|min:2|max:60', 
                'email'             => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',  
                'country_code'      =>  'required', 
                'mobile'            =>  ['required','numeric','digits_between:7,15',Rule::unique('users')->where(function($query) use($request){
                    $query->where(['country_code'=>$request->country_code,'deleted_at'=>NULL]);
                })],
                'password'          => 'required|min:8|max:45',
                'otp'               => 'nullable|numeric|digits_between:4,4',
                'device_type'       => 'required|in:IOS,ANDROID',
                'device_token'      => 'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator);
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $mobile = $request->country_code.$request->mobile;
                if($request->filled('otp')){  
                    $check = Otp::where(['mobile'=>$mobile,'otp'=>$request->otp])->first();
                    if($check){  
                        DB::beginTransaction();
                        $check->delete();  
                        $password = Hash::make($request->get('password'));   
                        $user = User::create([ 
                            'name'                  =>          trim($request->name), 
                            'email'                 =>          $request->email, 
                            'country_code'          =>          $request->country_code,
                            'mobile'                =>          $request->mobile,
                            'password'              =>          $password,
                            'status'                =>          1 
                        ]);  
                        $user = User::getProfile($user->id); 
                        if($user){
                            UserDevices::deviceHandle([
                                "id"            =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        } 
                        $security_token = $user->createToken($request->device_type)->plainTextToken;  
                        DB::commit();
                        return response()->json(['status' => 'true', 'message' => "Logged In!",'data'=>$user,'security_token'=>$security_token]);  
                    }else{
                        $message = __("Invalid or incorrect OTP.");
                        return response()->json(['status' => 'false', 'message' => $message,'data'=>[]]);
                    }      
                }else{
                    // $otp = mt_rand(1000, 9999);  
                    $otp = 1234;  
                    $message = "${otp} is your one time password to proceed on Ezyfix.\nThank you,\nTeam Ezyfix.";
                    Otp::where('mobile',$mobile)->delete();
                    Otp::create(['mobile'=>$mobile,'otp'=>$otp]); 
                    $data['otp'] = $otp;
                    //Twilio::sendMessage($mobile,$message);
                    return response()->json(['status' => 'true', 'message' => __("Otp sent!"),'data'=>$data]);
                }  
            }  
        } catch (\Exception $e) { 
            DB::rollback();
            return response()->json(['status' => 'false', 'message' =>$e->getLine().":".$e->getMessage(),'data'=>[]]); 
        }                     
    }

    // This method use for signin
    protected function signin(Request $request){
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'email'         => 'required',
                'password'      => 'nullable',
                'otp'               => 'nullable|numeric|digits_between:4,4',
                'device_type'   => 'required|in:IOS,ANDROID',
                'device_token'  => 'required',
            ]);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            } else { 
                if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) { 
                    $user = User::where(['email'=>$request->email])->first();
                } else { 
                    $ip = $request->ip();
                    // $ip = '110.235.219.239';
                    $geoplugin = file_get_contents("http://geoplugin.net/json.gp?ip=${ip}");
                    $geoplugin = json_decode($geoplugin); 
                    if(isset($geoplugin->geoplugin_status) && $geoplugin->geoplugin_status == 200){
                        $country = Country::where('iso',$geoplugin->geoplugin_countryCode)->first(); 
                        if($country){
                           $user = User::where(['country_code'=>$country->phonecode,'mobile'=>$request->email])->first(); 
                        }else{
                            return response()->json(['status' => 'false', 'message' => __("Unable to fetch country details, try login using your email."),'data'=>[]]);
                        }
                    }else{
                        return response()->json(['status' => 'false', 'message' => __("Unable to fetch country details, try login using your email."),'data'=>[]]);
                    }
                }            
                if(!$user){ 
                    return response()->json(['status' => 'false', 'message' => "Invalid email/phone or password",'data'=>[]]);
                }else{
                    if($user->status==0){ 
                        return response()->json(['status' => 'false', 'message' => __("Your account is inactive, please contact to administrator."),'data'=>[]]);
                    }
                    if(filter_var($request->email,FILTER_VALIDATE_EMAIL)){
                        if(!$request->filled('password')){
                            return response()->json(['status' => 'false', 'message' => "Invalid email/phone or password",'data'=>[]]);
                        }
                        if(Hash::check($data['password'],$user->password)){ 
                            UserDevices::deviceHandle([
                                "id"            =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                            // $user->tokens()->delete();
                            $security_token = $user->createToken($request->device_type)->plainTextToken; 
                            return response()->json(['status' => 'true', 'message' => __("Logged In!"),'data'=>$user,'security_token'=>$security_token]); 
                        }else{
                            return response()->json(['status' => 'false', 'message' => "Invalid email/phone or password",'data'=>[]]);
                        }
                    }else{
                        $mobile = $country->phonecode.$request->mobile;
                        if($request->filled('otp')){   
                            $check = Otp::where(['mobile'=>$mobile,'otp'=>$request->otp])->first();
                            if($check){  
                                UserDevices::deviceHandle([
                                    "id"            =>  $user->id,
                                    "device_type"   =>  $data['device_type'],
                                    "device_token"  =>  $data['device_token'],
                                ]); 
                                $security_token = $user->createToken($request->device_type)->plainTextToken;  
                                DB::commit();
                                return response()->json(['status' => 'true', 'message' => "Logged In!",'data'=>$user,'security_token'=>$security_token]);  
                            }else{
                                $message = __("Invalid or incorrect OTP.");
                                return response()->json(['status' => 'false', 'message' => $message,'data'=>[]]);
                            }      
                        }else{
                            // $otp = mt_rand(1000, 9999);  
                            $otp = 1234;  
                            $message = "${otp} is your one time password to proceed on Ezyfix.\nThank you,\nTeam Ezyfix.";
                            Otp::where('mobile',$mobile)->delete();
                            Otp::create(['mobile'=>$mobile,'otp'=>$otp]); 
                            $data['otp'] = $otp;
                            //Twilio::sendMessage($mobile,$message);
                            return response()->json(['status' => 'true', 'message' => __("Otp sent!"),'data'=>$data]);
                        } 
                    }   
                }
            }
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }  
    }

    // This method use for get profile
    public function getProfile(Request $request){ 
        try{
            $userId = $request->user()->id; 
            $profile = User::getProfile($userId); 
            if($profile){ 
                return response()->json(['status' => 'true', 'message' => 'User Profile','data'=>$profile]);
            }else{
                return response()->json(['status' => 'false', 'message' => __("User Not Found"),'data'=>[]]);   
            } 
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]);         
        }  
    }

    // This method use for update profile
    public function updateProfile(Request $request){ 
        try {  
            $data = $request->all();
            $userId = $request->user()->id; 
            $validator = Validator::make($request->all(), [
                'name'              => 'required|min:2|max:60',    
                'profile_picture'   => 'nullable|mimes:jpeg,bmp,png,jpg', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]);  
            }else{  
                $user = User::find($userId);
                $user->name = trim($data['name']);    
                if($request->file('profile_picture')!==null){
                    $destinationPath = '/uploads/profile/';
                    $responseData = Uploader::doUpload($request->file('profile_picture'),$destinationPath,true,100);    
                    if($responseData['status']=="true"){ 
                        $user->profile_picture = $responseData['file']; 
                        $user->thumb = $responseData['thumb']; 
                    }                             
                }                          
                if($user->save()){           
                    return response()->json(['status' => 'true', 'message' => __("Profile updated successfully."),'data'=>$user]);
                }else{ 
                    return response()->json(['status' => 'false', 'message' => __("Unknown error accured while updating information."),'data'=>[]]);
                }
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]);         
        } 
    }

    // This method use for change password
    public function changePassword(Request $request){
        try {
            $data = $request->all();
            $validator = Validator::make($request->all(), [
                'current_password'          => 'required|min:8|max:45',
                'new_password'              => 'required|min:8|max:45',
                'confirm_password'          => 'required|same:new_password',
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $userId = $request->user()->id; 
                $user = User::find($userId);
                if(!$user){
                    return response()->json(['status' => 'false', 'message' => __("User Not Found."),'data'=>[]]);
                }else{
                    if(Hash::check($data['current_password'],$user->password)){
                        $user->password = bcrypt($data['new_password']);
                        $user->save();
                        return response()->json(['status' => 'true', 'message' => __("Password change successfully."),'data'=>[]]);
                    }else{ 
                        return response()->json(['status' => 'false', 'message' => __("Current Password doesn't match."),'data'=>[]]);
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    // This method use for forgot password
    public function forgot(Request $request){
        try {
            $data = $request->all(); 
            $validator = Validator::make($data, [
                'email'         => 'required|email',
                'password'      => 'required_with:otp|min:8|max:45|confirmed', 
            ]);
            if($validator->fails()){ 
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $user = User::where('email',$data['email'])->first();
                if(!$user){
                    return response()->json(['status' => 'false', 'message' => __("Email does not exist.")]); 
                }else{
                    if($user->status==0){ 
                        return response()->json(['status' => 'false', 'message' => __("Your account is inactive, please contact to administrator."),'data'=>[]]);
                    }else{
                        if($request->filled('otp')){
                            $check = Otp::where(['email'=>$request->email,'otp'=>$request->otp])->first();
                            if($check){  
                                $check->delete();
                                $password = Hash::make($request->get('password'));  
                                $user->update(['password'=>$password]); 
                                return response()->json(['status' => 'true', 'message' => __("Password changed!"),'data'=>[]]);  
                            }else{
                                $message = __("Invalid or incorrect OTP.");
                                return response()->json(['status' => 'false', 'message' => $message,'data'=>[]]);
                            }
                        }else{
                            // $otp = mt_rand(1000, 9999); 
                            $otp = 1234; 
                            $email_data['name'] = $user->name;
                            $email_data['otp'] = $otp;
                            Otp::where('email',$request->get('email'))->delete();
                            Otp::create(['email'=>$request->get('email'),'otp'=>$otp]);
                            $title = "${otp} is your Ezyfix recovery code";
                            Email::send('send-otp',$email_data,$request->get('email'),$title);  
                            $data['otp'] = $otp;  
                            return response()->json(['status' => 'true', 'message' => __("Otp Sent!"),'data'=>$data]); 
                        } 
                    }
                }
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    // This method use for logout user.
    public function logout(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'device_type'       => 'required|in:ANDROID,IOS',
                'device_token'      => 'required',
            ]); 
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{    
                $userDevice = UserDevices::where(['device_type'=> $request['device_type'],'device_token'=>$request['device_token']])->first();
                if($userDevice){
                    $userDevice->delete();
                }
                $user = $request->user();
                $request->user()->currentAccessToken()->delete();
                return response()->json(['status' => 'true', 'message' => __("Logout Successfully.")]);
            }  
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }     
    }

    // list of country
    public function getCountry(){
        try{ 
            $countries = Cache::rememberForever('countries', function () {
                return Country::all();
            }); 
            return response()->json(['status' => 'true', 'message' => 'Country List Data','data'=>$countries]);
        }catch(\Exception $e){ 
            return response()->json(['status' => 'true', 'message' => $e->getMessage(),'data'=>[]]);
        }
    }

    public function setNotificationStatus(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'status'          =>       'required|in:0,1',
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $userId = $request->user()->id; 
                $status = $request->get('status');
                $user = User::find($userId);
                $user->update(['notification'=>$status]);
                if($user){
                    return response()->json(['status' => 'true', 'message' => __("Notification status updated successfully."),'data'=>$user]);
                }else{
                    return response()->json(['status' => 'false', 'message' => __("Error while updatig status, please try again."),'data'=>[]]);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getStaticPage(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'slug'          =>       'required',
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $slug = $request->get('slug');
                $page = Page::where('slug',$slug)->select('id','title','content')->first();
                if($page){
                    return response()->json(['status' => 'true', 'message' => 'Static page data.','data'=>$page]);
                }else{
                    return response()->json(['status' => 'false', 'message' => 'Invalid slug passed.','data'=>[]]);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }
  

    public function getNotifications(Request $request){ 
        try {               
            $userId = $request->user()->id;    
            $notifications = Notification::where('user_id',$userId)->orderBy('id','desc')->paginate(15)->toArray(); 
            return response()->json(['status' => 'true', 'message' => "Notification List",'data'=>$notifications]);    
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }
  

    public function getSettings(Request $request){ 
        try { 
            $settings = Setting::all()->pluck('value','field_name')->toArray();
            return response()->json(['status' => 'true', 'message' => 'Data.','data'=>$settings]);
              
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }
   

    public function getAppSetting(Request $request){
        try {  
            $app_settings = Cache::rememberForever('app_settings', function (){
                return  AppSetting::where('id','1')->first();
            }); 
            return response()->json(['status'=>'true','message'=>__('App Setting'),'data'=>$app_settings]); 
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getCategories(Request $request){
        try {  
            $categories = Cache::rememberForever('categories', function (){
                return  Category::where('status',1)->select('id','title','image','description')->orderBy('position','desc')->get();
            }); 
            return response()->json(['status'=>'true','message'=>"Categories",'data'=>$categories]); 
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getNearbyVendors(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'latitude'              =>  'required',
                'longitude'             =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $lat = $request->get('latitude');       
                $long = $request->get('longitude'); 
                $gr_circle_radius = 6371; 
                $distance_select = sprintf(
                    "( %d * acos( cos( radians(%s) ) " .
                            " * cos( radians( latitude ) ) " .
                            " * cos( radians( longitude ) - radians(%s) ) " .
                            " + sin( radians(%s) ) * sin( radians( latitude ) ) " .
                    " ) " .")",
                $gr_circle_radius,               
                $lat,
                $long,
                $lat
                );   
                $vendors = Vendor::where(['status'=>1,['latitude','!=',''],['longitude','!=','']])->whereHas('giftcards', function ($query) use($request) {
                    $query->where('status','1');
                    if($request->filled('category_id')){
                        $query->where('category_id',$request->category_id);
                    }
                })->select([DB::raw("id,name,latitude,longitude,address,logo, $distance_select AS distance")])->selectRaw("(SELECT AVG(rating) from vendor_ratings where vendor_ratings.vendor_id=vendors.id) as average_rating")->selectRaw("(SELECT AVG(price) from gift_cards where gift_cards.vendor_id=vendors.id and gift_cards.status = 1) as average_price"); 
                if($request->filled('search')){
                    $vendors = $vendors->where(function($q) use($request){
                        $q->where('name','like','%'.$request->search.'%');
                        $q->orWhere('description','like','%'.$request->search.'%');
                        $q->orWhere('address','like','%'.$request->search.'%');
                    });
                }
                if($request->filled('order')){
                    switch ($request->order) {
                        case 'TOP_RATED': 
                            $vendors = $vendors->orderBy('average_rating','desc');
                            break;
                        case 'PRICE_LOW_HIGH': 
                            $vendors = $vendors->orderBy('average_price','asc');
                            break;
                        case 'PRICE_HIGH_LOW': 
                            $vendors = $vendors->orderBy('average_price','desc');
                            break; 
                        default:
                            // code...
                            break;
                    }
                }
                $vendors = $vendors->orderBy('distance','asc')->paginate(20)->toArray();
                /*foreach($vendors['data'] as $key => $vendor){
                    $vendors['data'][$key]['rating'] = rand(0,5);
                }*/                
                return response()->json(['status' => 'true', 'message' => 'Nearby Vendors Data','data'=>$vendors]); 
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getVendorDetails(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'id'              =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $vendor = Vendor::with(['images'=>function($q){
                    $q->select('id','vendor_id','image');
                }])->where(['id'=>$request->id,'status'=>1])->firstorfail();         
                return response()->json(['status' => 'true', 'message' => 'Vendors Data','data'=>$vendor]); 
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getVendorGiftcards(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'id'              =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $userId = $request->user()->id; 
                $giftcards = GiftCard::where(['vendor_id'=>$request->id,'status'=>1])->with(['category'=>function($q){
                    $q->select('id','title','image');
                },'order'=>function($q) use($userId){
                    $q->where(['user_id'=>$userId]);
                }])->orderBy('id','desc')->paginate(20); 
                return response()->json(['status' => 'true', 'message' => 'Vendors Giftcards','data'=>$giftcards]); 
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getVendorReviews(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'id'              =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $reviews = VendorRating::where(['vendor_id'=>$request->id])->with(['user'=>function($q){
                    $q->select('id','name','profile_picture');
                }])->orderBy('id','desc')->paginate(20); 
                return response()->json(['status' => 'true', 'message' => 'Vendors Reviews','data'=>$reviews]); 
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function addAddress(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'city'              =>  'required',
                'zip_code'          =>  'required',
                'address_type'      =>  'required',
                'sublocality'       =>  'required',
                'lat'               =>  'required',
                'lng'               =>  'required',
                'country'           =>  'required',
                'address'           =>  'required'
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                DB::beginTransaction();
                $userId = $request->user()->id;
                UserAddress::where('user_id',$userId)->update(['is_default'=>0]); 
                UserAddress::updateOrCreate(['id'=>($request->filled('id'))?$request->id:null],[
                    'user_id'           =>          $userId,
                    'address'           =>          $request->address,
                    'city'              =>          $request->city,
                    'zip_code'          =>          $request->zip_code,
                    'address_type'      =>          $request->address_type,
                    'sublocality'       =>          $request->sublocality,
                    'lat'               =>          $request->lat,
                    'lng'               =>          $request->lng,
                    'country'           =>          $request->country,
                    'address_type_desc' =>          ($request->filled('address_type_desc'))?$request->address_type_desc:null,
                    'is_default'        =>          1
                ]);
                DB::commit();
                return response()->json(['status' => 'true', 'message' => 'Address saved!','data'=>[]]); 
            }
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getAddresses(Request $request){
        try {    
            $userId = $request->user()->id;
            $addresses = UserAddress::where('user_id',$userId)->orderBy('id','desc')->get();  
            return response()->json(['status' => 'true', 'message' => 'Addresses!','data'=>$addresses]); 
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getDefaultAddress(Request $request){
        try {    
            $userId = $request->user()->id;
            $address = UserAddress::where(['user_id'=>$userId,'is_default'=>1])->first();  
            return response()->json(['status' => 'true', 'message' => 'Addresses!','data'=>$address]); 
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function setDefaultAddress(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'id'              =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $userId = $request->user()->id;
                $address = UserAddress::where('user_id',$userId)->where('id',$request->id)->firstorfail(); 
                $address->is_default = 1;
                $address->save();  
                return response()->json(['status' => 'true', 'message' => 'Default address saved!','data'=>[]]); 
            }
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function deleteAddress(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'id'              =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $userId = $request->user()->id;
                $address = UserAddress::where('user_id',$userId)->where('id',$request->id)->firstorfail();  
                if($address->is_default=='1'){
                    return response()->json(['status' => 'false', 'message' => 'You can not delete default address.','data'=>[]]);
                }
                $address->delete();  
                return response()->json(['status' => 'true', 'message' => 'Address deleted!','data'=>[]]); 
            }
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function redeemGiftCard(Request $request){
        try {  
            $data = $request->all();
            $validator = Validator::make($data, [
                'giftcard_id'            =>  'required', 
                'vendor_id'              =>  'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $userId = $request->user()->id; 
                $giftcard = GiftCard::where(['id'=>$request->giftcard_id,'vendor_id'=>$request->vendor_id,'status'=>1])->first();
                if($giftcard){ 
                    if($giftcard->validity_type == 'MANUAL'){
                        $startDate = \Carbon\Carbon::createFromFormat('Y-m-d',$giftcard->start_date);
                        $endDate = \Carbon\Carbon::createFromFormat('Y-m-d',$giftcard->end_date);
                        $check = \Carbon\Carbon::now()->between($startDate,$endDate);
                        if(!$check){
                            return response()->json(['status' => 'false', 'message' => 'Something went wrong, please try again.','data'=>[]]); 
                        }
                    }
                    $code = $this->generateUniqueCode(); 
                    $order = Order::create([
                        'user_id'       =>      $request->user()->id,
                        'giftcard_id'   =>      $giftcard->id,
                        'vendor_id'     =>      $request->vendor_id,
                        'code'          =>      $code
                    ]);
                    return response()->json(['status' => 'true', 'message' => 'Code generated successfully.','data'=>$order]);
                }else{
                    return response()->json(['status' => 'false', 'message' => 'Something went wrong, please try again.','data'=>[]]); 
                } 
            }
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function myOrders(Request $request){
        try {   
            $userId = $request->user()->id; 
            $orders = Order::where(['user_id'=>$userId])->with(['giftcard'])->orderBy('id','desc')->paginate(15);
            return response()->json(['status' => 'true', 'message' => 'My orders.','data'=>$orders]);
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function generateUniqueCode()
    {
        do {
            $code = strtoupper(Str::random(8));
        } while (Order::where("code", "=", $code)->first());
  
        return $code;
    }

    

    
 
   
}
