<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GiftCard;
use App\Models\Category;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str;  
use Auth;  
class GiftcardController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        $vendor_id = Auth::guard('vendor')->id(); 
        if($id){
            $title = "Edit Gift Card";
            $breadcrumbs = [
                ['name'=>'Gift Card','relation'=>'link','url'=>route('vendor.giftcards.index')],
                ['name'=>'Edit Gift Card','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Gift Card";
            $breadcrumbs = [
                ['name'=>'Gift Card','relation'=>'link','url'=>route('vendor.giftcards.index')],
                ['name'=>'Add New Gift Card','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?GiftCard::where(['uuid'=>$id,'vendor_id'=>$vendor_id])->firstOrFail():array();
        if($data && $data->validity_type=='MANUAL'){ 
            $data->start_date = date("d-m-Y",strtotime($data->start_date));
            $data->end_date = date("d-m-Y",strtotime($data->end_date));
        }
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [     
                    'category_id'       =>  'required|exists:categories,id',      
                    'title'             =>  'required|max:150',      
                    'display_price'     =>  'required|numeric|min:1',      
                    'price'             =>  'required|numeric|min:1|lt:display_price',      
                    'validity_type'     =>  'required|in:ALL_DAYS,MANUAL',  
                    'start_date'        =>  'exclude_if:validity_type,ALL_DAYS|required|date_format:d-m-Y',  
                    'end_date'          =>  'exclude_if:validity_type,ALL_DAYS|required|date_format:d-m-Y|after:start_date',  
                    'start_time'        =>  'required',  
                    'end_time'          =>  'required|after:start_time',  
                    'image'             =>  'nullable|image', 
                    'description'       =>  'required|max:2000', 
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except('image');
                    if($request->validity_type=='ALL_DAYS'){
                        $formData['start_date'] = null;
                        $formData['end_date'] = null;
                    }else{
                        $formData['start_date'] = date("Y-m-d",strtotime($request->start_date));
                        $formData['end_date'] = date("Y-m-d",strtotime($request->end_date));
                    } 
                    $formData['formated_start_time'] = date("H:i:s",strtotime($request->start_time)); 
                    $formData['formated_end_time'] = date("H:i:s",strtotime($request->end_time)); 
                    if($request->file('image')!==null){ 
                        $destinationPath = "/uploads/giftcards/$vendor_id";
                        $responseData = Uploader::doUpload($request->file('image'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['image'] = $responseData['file']; 
                        }                             
                    }  
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','GiftCard updated successfully.');
                    }else{  
                        $formData['uuid'] = Str::orderedUuid();
                        $formData['vendor_id'] = $vendor_id;
                        GiftCard::create($formData);
                        Session::flash('success','GiftCard created successfully.');
                    } 
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }
        $categories = Category::where(['status'=>'1'])->orderBy('title','asc')->pluck('title','id')->toArray();
        return view('vendor/giftcards/add',compact('id','data','title','breadcrumbs','categories'));
    }

    public function index(){ 
        $title = "Gift Card";
        $breadcrumbs = [ 
            ['name'=>'Gift Card','relation'=>'Current','url'=>'']
        ]; 
        return view('vendor/giftcards/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        $pages = GiftCard::select(['id','uuid', 'title','price','validity_type', 'status','image','category_id'])->with(['category'])->where('vendor_id',Auth::guard('vendor')->id()); 
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('vendor.giftcards.add',$page->uuid).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('vendor.giftcards.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('vendor.giftcards.status'));
            })
            ->editColumn('price',function($page){
                return '$'.number_format($page->price,2);
            })
            ->editColumn('validity_type',function($page){
                return str_replace("_"," ",$page->validity_type);
            }) 
            ->editColumn('image',function($page){
                return Helper::getImage($page->image,100);
            }) 
            ->rawColumns(['status','action','image','category_id'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            $vendor_id = Auth::guard('vendor')->id();
            $row = GiftCard::where('id',$id)->where('vendor_id',$vendor_id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('vendor.giftcards.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            $vendor_id = Auth::guard('vendor')->id();
            try{
                $delete = GiftCard::where('id','=',$id)->where('vendor_id',$vendor_id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
