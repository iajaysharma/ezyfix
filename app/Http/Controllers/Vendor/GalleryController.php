<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\VendorImage;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str;  
use Auth;  
use Response; 
use DB; 
class GalleryController extends Controller
{
    public function __construct()
    {
        
    }

    public function images(Request $request){ 
        $title = "Upload Gallery Images";   
        return view('vendor/gallery/images',compact('title')); 
    }

    public function getImages(Request $request){
        try {
            if($request->ajax() && $request->isMethod('get')){
                $images = VendorImage::where('vendor_id',Auth::guard('vendor')->id())->orderBy('id','desc')->get()->toArray();
                if(count($images)>0){
                    return ['status' => true, 'message' => "Wine Images", 'data'=>$images];
                }else{
                    return ['status' => false, 'message' => "No Images Found."];
                }
            }
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function uploadImages(Request $request){ 
        try {
            if($request->ajax() && $request->isMethod('post')){
                DB::beginTransaction();
                $rules = [
                    'file'         => 'required|image|max:5000',  
                ]; 
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return Response::make($validator->messages()->first(), 400);
                }else{
                    if($request->file('file')!==null){
                        $vendor_id = Auth::guard('vendor')->id();
                        // This code use for profile picture upload
                        $destinationPath = "/uploads/gallery/$vendor_id/";
                        $responseData = Uploader::doUpload($request->file('file'),$destinationPath,false); 
                        if($responseData['status']=="true"){
                            VendorImage::create([
                                'uuid'          =>      Str::orderedUuid(),
                                'vendor_id'     =>      $vendor_id,
                                'image'         =>      $responseData['file']

                            ]);
                            DB::commit();
                            return Response::json('success', 200);
                        }else{
                            return Response::json('error', 400);
                        }                             
                    }               
                }
            }else{
                DB::rollback();
                return Response::json('error', 400);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json($e->getMessage(), 400);
        } 
    }

    public function deleteImage(Request $request)
    {
        $id = $request->id;
        try{
            $vendor_id = Auth::guard('vendor')->id();
            $delete = VendorImage::where('id','=',$id)->where('vendor_id',$vendor_id)->delete();   
            if($delete){
                echo json_encode(["type"=>"success","data"=>"Record Deleted"]); 
            }else{
                echo json_encode(["type"=>"error","data"=>"Could not deleted Record"]); 
            }
        }catch(\Exception $e){
            echo json_encode(["type"=>"error","data"=>$e->getMessage()]);   
        }
    }
}
