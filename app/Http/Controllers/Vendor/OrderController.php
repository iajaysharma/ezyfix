<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GiftCard;
use App\Models\Order;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str;  
use Auth;  
class OrderController extends Controller
{
    public function __construct()
    {
        
    } 


    public function index(){ 
        $title = "Orders";
        $breadcrumbs = [ 
            ['name'=>'Orders','relation'=>'Current','url'=>'']
        ]; 
        return view('vendor/orders/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        // ->where('vendor_id',Auth::guard('vendor')->id())
        $pages = Order::select(['id','uuid', 'user_id','giftcard_id','code', 'is_vendor_approved','created_at'])->with(['user','giftcard'])->where('vendor_id',Auth::guard('vendor')->id()); 
        return DataTables::of($pages) 
            /*->addColumn('action', function ($page) {
                return '<a href="'.route('vendor.giftcards.add',$page->uuid).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('vendor.giftcards.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })*/
            ->editColumn('is_vendor_approved',function($page){
                return Helper::getVendorApprovalStatus($page->is_vendor_approved,$page->id,route('vendor.orders.approval'));
            })
            ->editColumn('created_at',function($page){
                return date("Y-m-d h:i A",strtotime($page->created_at));
            })  
            ->rawColumns(['is_vendor_approved','user_id','giftcard_id','created_at'])
            ->make(true);
    }

    public function approval(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Order::where('id',$id)->first();
            $row->is_vendor_approved = 1;
            $row->save(); 
            return Helper::getVendorApprovalStatus($row->is_vendor_approved,$id,route('vendor.orders.approval'));
        }
    }  

     
}
