<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Models\GiftCard;   
use App\Models\VendorImage;   
use App\Models\Vendor;   
use App\Http\Controllers\Controller;
use Auth;
use Session;
use Validator;
use Hash;
use App\Lib\Uploader;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { 

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $title = "Dashboard";
        $auth_id = Auth::guard('vendor')->id();
        $total_giftcards = GiftCard::where(['vendor_id'=>$auth_id])->count();
        $total_images = VendorImage::where(['vendor_id'=>$auth_id])->count();
        return view('vendor.home.index',compact('title','total_giftcards','total_images'));  
    }

    public function profile(Request $request){ 
        $title = "Profile";
        $breadcrumbs = [
            ['name'=>'Profile','relation'=>'Current','url'=>'']
        ];
        $data = Vendor::find(Auth::guard('vendor')->user()->id);
        if($request->ajax() && $request->isMethod('post')){
            try {
                $validator = Validator::make($request->all(), [
                    'name'              => 'required|max:45',       
                    'address'           =>  'required|max:300', 
                    'latitude'          =>  'required', 
                    'longitude'         =>  'required', 
                    'phone'             =>  'required|digits_between:7,15', 
                    'logo'              =>  'nullable|image', 
                ]);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = [
                        'name'      =>      $request->get('name'), 
                        'address'   =>      $request->address,
                        'latitude'   =>      $request->latitude,
                        'longitude'   =>      $request->longitude,
                        'phone'   =>      $request->phone,
                    ];
                    if ($request->hasFile('logo')) {
                        if($request->file('logo')->isValid()) { 
                            $path = "/uploads/vendors/";
                            $responseData =  Uploader::doUpload($request->file('logo'),$path,false);
                            $formData['logo'] = $responseData['file'];
                        }
                    } 
                    $data->update($formData);
                    return ['status'=>'true','message'=>__("Profile updated successfully.")];
                }
            } catch (\Exception $e) {
                return ['status'=>'false','message'=>$e->getMessage()];
            }
        }
        return view('vendor.home.profile',compact('title','breadcrumbs','data'));
    }

    public function changePassword(Request $request)
    { 
        $title = "Change Password";
        $breadcrumbs = [
            ['name'=>'Change Password','relation'=>'Current','url'=>'']
        ];
        if($request->ajax() && $request->isMethod('post')){
            try {
                $validator = Validator::make($request->all(), [
                    'current_password'      => 'required|max:45',     
                    'new_password'          => 'required|max:45|min:8|same:confirm_password',     
                    'confirm_password'      => 'required|max:45|min:8'
                ]);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{ 
                    $data = Vendor::find(Auth::guard('vendor')->user()->id);
                    if(Hash::check($request->get('current_password'),$data->password)){
                        $data->update(['password'=>Hash::make($request->get('new_password'))]); 
                        Auth::logoutOtherDevices($request->new_password);
                        Session::flash('success',__("Password updated successfully."));
                        return ['status'=>'true','message'=>__("Password updated successfully.")];
                    }else{
                        return ['status'=>'false','message'=>__("Current password does't match.")];
                    }         
                }
            } catch (\Exception $e) {
                return ['status'=>'false','message'=>$e->getMessage()];
            }
        } 
        return view('vendor.home.change_password',compact('title','breadcrumbs'));
    }

    function toggleSidebar(Request $request){
        if($request->ajax() && $request->isMethod('get')){
            try {
                $sidebar_style = Auth::guard('vendor')->user()->sidebar_style;
                $sidebar_style = ($sidebar_style=='MINI')?'FULL':'MINI';
                Vendor::where('id',Auth::guard('vendor')->id())->update(['sidebar_style'=>$sidebar_style]);
                return ['status'=>'true','message'=>'Settings updated'];
            } catch (\Exception $e) {
                return ['status'=>'false','message'=>$e->getMessage()];
            }

        }
    }
}
