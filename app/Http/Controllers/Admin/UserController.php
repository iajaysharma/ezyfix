<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Notification;
use App\Models\Language;
use App\Models\CallHistory;
use App\Models\AppSetting;
use App\Models\Subscription;
use Auth; 
use App\Lib\Helper;
use Validator;
use Session;
use Hash;
use DataTables;
use DB;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(){ 
        $title = "Users";
        $breadcrumbs = [ 
            ['name'=>'Users','relation'=>'Current','url'=>'']
        ];
        return view('admin/users/index',compact('title','breadcrumbs'));
    }

    public function datatables()
    {
        $users = User::select(['id', 'name','country_code','mobile', 'email', 'status','created_at'])->get();

        return DataTables::of($users)
            ->addColumn('action', function ($user) {
                return '<a target="_blank" href="'.route('admin.users.view',$user->id).'" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i> View</a>&nbsp;<a data-link="'.route('admin.users.delete').'" id="delete_'.$user->id.'" onclick="confirm_delete('.$user->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
              
            ->editColumn('status',function($user){
                return Helper::getStatus($user->status,$user->id,route('admin.users.status'));
            })   
            ->editColumn('created_at',function($user){
                return date("Y-m-d",strtotime($user->created_at));
            })
            ->editColumn('mobile',function($user){
                return "+".$user->country_code."-".$user->mobile;
            })    
            ->rawColumns(['status','action','name','mobile'])
            ->make(true);
    }

    public function status(Request $request)
    {
        $id = $request->id; 
        $row = User::whereId($id)->first();
        $row->status = $row->status=='1'?'0':'1';
        $row->save(); 
        return Helper::getStatus($row->status,$id,route('admin.users.status')); 
    }
    
    public function view($id){
        $data = User::where('id',$id)->first(); 
        if($data){
            $title = "Profile - " .$data->first_name;
            $breadcrumbs = [ 
                ['name'=>"Users",'relation'=>'link','url'=>route('admin.users.index')],
                ['name'=>$title,'relation'=>'Current','url'=>'']
            ];
            return view('admin/users/view',compact('title','breadcrumbs','data'));
        }else{
            return abort(404);
        }
    }

    public function delete(Request $request)
    {
        $user_id = $request->id;
        try{
            $delete = User::where('id','=',$user_id)->delete();   
            if($delete){ 
                return ["status"=>"true","message"=>"Record Deleted"]; 
            }else{
                return ["status"=>"error","message"=>"Could not deleted Record"]; 
            }
        }catch(\Exception $e){
            return ["status"=>"error","message"=>$e->getMessage()];   
        }
    }


    public function appSetting(Request $request){ 
        $title = "App Setting";
        $breadcrumbs = [ 
            ['name'=>'App Setting','relation'=>'Current','url'=>'']
        ]; 
        $data = AppSetting::where('id','1')->first();  
        if($request->ajax() && $request->isMethod('post')){
            try {
                $validator = Validator::make($request->all(), [
                    'android_version'   => 'required|numeric', 
                    'ios_version'       =>'required|numeric'  
                ]);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except('_token');   
                    AppSetting::where('id','1')->update($formData);   
                    return ['status' => 'true', 'message' => 'App Setting updated successfully']; 
                }
            } catch (\Exception $e) {
                return ['status' => 'false', 'message' => $e->getMessage()];
            }            
        } 
        return view('admin/users/app-setting',compact('data','title','breadcrumbs'));
    }

    
}
