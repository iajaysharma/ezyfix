<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\User;
use App\Lib\Helper;
use Session;
use DataTables;
use Validator;
use App\Models\Notification;
use App\Models\NotificationStatus;
use App\Lib\PushNotification;
class SendNotificationController extends Controller
{
    public function __construct()
    {
        
    }

    public function send(Request $request){
        
            $title = "Send Notification";
            $breadcrumbs = [
                ['name'=>'Send Notification','relation'=>'link','url'=>route('admin.notification.index')],
                ['name'=>'Send Notification','relation'=>'Current','url'=>'']
            ];
        
        if($request->ajax() && $request->isMethod('post')){
            try {
                $validator = Validator::make($request->all(), [
                    'user_type'=>'required',     
                    'title'=>'nullable|max:255',     
                    'content'           => 'required|max:600',  
                ]);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->all();
                    if($formData['user_type'] == 'CPA'){
                        $userIds =  User::where('role','CPA')->where('status',1)->pluck('id')->toArray();
                    }
                    elseif ($formData['user_type'] == 'USER') {
                        $userIds =  User::where('role','USER')->where('status',1)->pluck('id')->toArray();
                    }
                    else{
                        $userIds =  User::where('status',1)->pluck('id')->toArray();
                    }   
                    $type = 'ADMIN_NOTIFICATION';
                    $message = $formData['content'];
                    $data = [];
                    // $create = true;
                    if(isset($request->title) && $request->title != null){
                        $title = $request['title'];
                    }
                    else{
                        $title="Simple Fun Tax";
                    }
                    

                    if(count($userIds)>0){
                        $notificatin = Notification::create([
                            'user_id' => 0,
                            'notification_type' => $type,
                            'title' => $title,  
                            'message' => $message,  
                            'is_seen' => '0',
                            'data' => json_encode($data), 
                        ]);
                        @PushNotification::Notify($userIds, $message, $type, $data,$title);
                        foreach ($userIds as $key => $value) {
                            # code...
                           $notiStatus = NotificationStatus::create([
                                'user_id' => $value,
                                'notification_id' => $notificatin->id,
                                'is_seen'=>0
                           ]);
                        }
                    }
                    // dd($userIds);  
                    Session::flash('success','Notification send successfully.');  
                    return ['status' => 'true', 'message' => 'Notification send successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => 'false', 'message' => $e->getMessage()];
            }            
        } 
        return view('admin/send-notification/add',compact('title','breadcrumbs'));
    }


     public function index(){ 
        $title = "Custom Notification";
        $breadcrumbs = [ 
            ['name'=>'Custom Notification','relation'=>'Current','url'=>'']
        ];
        return view('admin/send-notification/index',compact('title','breadcrumbs'));
    }

    public function datatables()
    {
        $pages = Notification::select(['id', 'title', 'message', 'created_at'])->where('notification_type','ADMIN_NOTIFICATION')->get();

        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a data-link="'.route('admin.notification.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('created_at',function($page){
                return date("Y-m-d",strtotime($page->created_at));
            })  
            ->rawColumns(['created_at','action'])
            ->make(true);
    }


    public function delete(Request $request)
    {
        $notification_id = $request->id;
        try{
            $delete = Notification::where('id','=',$notification_id)->delete();
            $statusdelete = NotificationStatus::where('notification_id','=',$notification_id)->delete();   

            if($delete){ 
                return ["status"=>"true","message"=>"Record Deleted"]; 
            }else{
                return ["status"=>"error","message"=>"Could not deleted Record"]; 
            }
        }catch(\Exception $e){
            return ["status"=>"error","message"=>$e->getMessage()];   
        }
    }

}
