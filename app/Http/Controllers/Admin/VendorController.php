<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vendor;
use App\Lib\Helper;
use App\Lib\Uploader;
use App\Lib\Email;
use Session;
use DataTables;
use Validator;  
use DB;
use Hash;
class VendorController extends Controller
{


    public function add(Request $request,$id=null){ 
        if($id){
            $title = "Edit Vendor";
            $breadcrumbs = [
                ['name'=>'Vendor','relation'=>'link','url'=>route('admin.vendors.index')],
                ['name'=>'Edit Vendor','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Vendor";
            $breadcrumbs = [
                ['name'=>'Vendor','relation'=>'link','url'=>route('admin.vendors.index')],
                ['name'=>'Add New Vendor','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Vendor::find($id):array(); 
        if($request->ajax() && $request->isMethod('post')){
            DB::beginTransaction();
            try {
                $rules = [     
                    'name'              =>  'required|max:200',      
                    'email'             =>  'required|email|unique:vendors,email,'.$id.',id,deleted_at,NULL',       
                    'address'           =>  'required|max:300', 
                    'latitude'          =>  'required', 
                    'longitude'         =>  'required', 
                    'phone'             =>  'required|digits_between:7,15', 
                    'logo'              =>  'nullable|image', 
                ];
                if(!$id){
                    $rules['password'] = 'required|min:8|max:45';
                }
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except('logo');   
                    if($request->file('logo')!==null){ 
                        $destinationPath = '/uploads/vendors/';
                        $responseData = Uploader::doUpload($request->file('logo'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['logo'] = $responseData['file']; 
                        }                             
                    } 
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','Vendor updated successfully.');
                    }else{  
                        $formData['password'] = Hash::make($request->password);
                        Vendor::create($formData);
                        $email_data = [
                            'name'       =>      $request->name,
                            'email'      =>      $request->email,
                            'password'   =>      $request->password,
                            'url'        =>      "<a class='btn btn-link' hreh='".url('/')."'>".url('/')."</a>",
                        ];
                        Email::send('new-vendor-registration',$email_data,$request->email,"Welcome to ".env("APP_NAME"));
                        Session::flash('success','Vendor created successfully.');
                    }
                    DB::commit();
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                DB::rollback();
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }  
        return view('admin/vendors/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Vendor";
        $breadcrumbs = [ 
            ['name'=>'Vendor','relation'=>'Current','url'=>'']
        ]; 
        return view('admin/vendors/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        $pages = Vendor::select(['id', 'name','email','phone','address', 'status']); 
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.vendors.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.vendors.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.vendors.status'));
            }) 
            ->rawColumns(['status','action'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Vendor::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.vendors.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Vendor::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
