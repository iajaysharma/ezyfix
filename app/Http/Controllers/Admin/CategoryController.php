<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str; 
use Cache;
class CategoryController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        if($id){
            $title = "Edit Category";
            $breadcrumbs = [
                ['name'=>'Category','relation'=>'link','url'=>route('admin.categories.index')],
                ['name'=>'Edit Category','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Category";
            $breadcrumbs = [
                ['name'=>'Category','relation'=>'link','url'=>route('admin.categories.index')],
                ['name'=>'Add New Category','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Category::find($id):array(); 
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [     
                    'title'             =>  'required|max:200',      
                    'image'             =>  'nullable|image',  
                    'position'          =>  'nullable|numeric',
                    'icon'              =>  'nullable|image', 
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except('image');  
                    if(empty($formData['position'])){
                        $formData['position'] = (int)Category::count()+1;
                    } 
                    if($request->file('image')!==null){ 
                        $destinationPath = '/uploads/categories/';
                        $responseData = Uploader::doUpload($request->file('image'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['image'] = $responseData['file']; 
                        }                             
                    }
                    if($request->file('icon')!==null){ 
                        $destinationPath = '/uploads/categories/';
                        $responseData = Uploader::doUpload($request->file('icon'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['icon'] = $responseData['file']; 
                        }                             
                    } 
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','Category updated successfully.');
                    }else{  
                        Category::create($formData);
                        Session::flash('success','Category created successfully.');
                    }
                    Cache::forget('categories');
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }  
        return view('admin/categories/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Category";
        $breadcrumbs = [ 
            ['name'=>'Category','relation'=>'Current','url'=>'']
        ];
        $parentCate = Category::where('parent_id',null)->pluck('title','id');
        return view('admin/categories/index',compact('title','breadcrumbs','parentCate'));
    }

    public function datatables(Request $request)
    {
        $pages = Category::select(['id', 'title','slug', 'status','image','position']);
        // dd($pages->toArray());
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.categories.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.categories.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.categories.status'));
            }) 
            ->editColumn('image',function($page){
                return Helper::getImage($page->image,100);
            }) 
            ->rawColumns(['status','action','image','parent'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Category::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.categories.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Category::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
