<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Review;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str; 
use Cache;
class ReviewController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        if($id){
            $title = "Edit Review";
            $breadcrumbs = [
                ['name'=>'Review','relation'=>'link','url'=>route('admin.reviews.index')],
                ['name'=>'Edit Review','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Review";
            $breadcrumbs = [
                ['name'=>'Review','relation'=>'link','url'=>route('admin.reviews.index')],
                ['name'=>'Add New Review','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Review::find($id):array(); 
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [ 
                    'username'          =>  'required|max:100',      
                    'title'             =>  'required|max:255',   
                    'description'       =>  'required',   
                    'rating'            =>  'required|numeric', 
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->all();   
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','Review updated successfully.');
                    }else{  
                        Review::create($formData);
                        Session::flash('success','Review created successfully.');
                    } 
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }  
        return view('admin/reviews/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Review";
        $breadcrumbs = [ 
            ['name'=>'Review','relation'=>'Current','url'=>'']
        ]; 
        return view('admin/reviews/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        $pages = Review::select(['id','username','title','description', 'status','rating']); 
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.reviews.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.reviews.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.reviews.status'));
            })  
            ->editColumn('rating',function($page){
                $rating = '';
                for($i=0;$i<$page->rating;$i++){
                    $rating .= "<i class='fa fa-star'></i>";
                }
                return $rating;
            }) 
            ->rawColumns(['status','action','rating'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Review::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.reviews.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Review::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
