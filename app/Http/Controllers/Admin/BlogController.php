<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str; 
use Cache;
class BlogController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        if($id){
            $title = "Edit Blog";
            $breadcrumbs = [
                ['name'=>'Blog','relation'=>'link','url'=>route('admin.blogs.index')],
                ['name'=>'Edit Blog','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Blog";
            $breadcrumbs = [
                ['name'=>'Blog','relation'=>'link','url'=>route('admin.blogs.index')],
                ['name'=>'Add New Blog','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Blog::find($id):array(); 
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [ 
                    'title'             =>  'required|max:200',      
                    'image'             =>  'nullable|image',   
                    'content'           =>  'required',
                    'meta_title'        =>  'nullable|max:255',
                    'meta_keywords'     =>  'nullable|max:255',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except('image');   
                    if($request->file('image')!==null){ 
                        $destinationPath = '/uploads/blogs/';
                        $responseData = Uploader::doUpload($request->file('image'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['image'] = $responseData['file']; 
                        }                             
                    } 
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','Blog updated successfully.');
                    }else{  
                        Blog::create($formData);
                        Session::flash('success','Blog created successfully.');
                    } 
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }  
        return view('admin/blogs/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Blog";
        $breadcrumbs = [ 
            ['name'=>'Blog','relation'=>'Current','url'=>'']
        ]; 
        return view('admin/blogs/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        $pages = Blog::select(['id', 'title','slug', 'status','image']);
        // dd($pages->toArray());
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.blogs.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.blogs.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.blogs.status'));
            }) 
            ->editColumn('image',function($page){
                return Helper::getImage($page->image,100);
            }) 
            ->rawColumns(['status','action','image'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Blog::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.blogs.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Blog::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
