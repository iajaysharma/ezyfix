<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Faq; 
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str; 
class FaqController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        if($id){
            $title = "Edit Faq";
            $breadcrumbs = [
                ['name'=>'Faq','relation'=>'link','url'=>route('admin.faqs.index')],
                ['name'=>'Edit Faq','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Faq";
            $breadcrumbs = [
                ['name'=>'Faq','relation'=>'link','url'=>route('admin.faqs.index')],
                ['name'=>'Add New Faq','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Faq::find($id):array();
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [
                    'title'                 => 'required|max:255',    
                    'description'           => 'required', 
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->all();
                    if($id){                        
                        $data->update($formData);
                        Session::flash('success','Faq updated successfully.');
                    }else{
                        Faq::create($formData);
                        Session::flash('success','Faq created successfully.');
                    }
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        } 
        return view('admin/faqs/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Faq";
        $breadcrumbs = [ 
            ['name'=>'Faq','relation'=>'Current','url'=>'']
        ];
        return view('admin/faqs/index',compact('title','breadcrumbs'));
    }

    public function datatables()
    {
        $pages = Faq::select(['id', 'title', 'status'])->get();
        return DataTables::of($pages)
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.faqs.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.faqs.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.faqs.status'));
            })
            ->rawColumns(['status','action'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Faq::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.faqs.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Faq::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
