<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Notification;
use App\Lib\PushNotification;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function validationHandle($validator)
    {
        /*foreach ($validation->getMessages() as $field_name => $messages){
            if(!isset($firstError)){
                $firstError = $messages[0];
            } 
        }*/
        return $validator->errors()->all()[0];  
    }

    public function sentNotification($user_id,$type, $message,$data=[],$title="Simple Fun Tax",$create=false){
        try {
            //Create notification
            // dd($create);
            if($create){
                Notification::create([
                    'user_id' => $user_id,
                    'notification_type' => $type,
                    'title' => $title,  
                    'message' => $message,  
                    'is_seen' => '0',
                    'data' => json_encode($data), 
                ]);
            }
            
            @PushNotification::Notify($user_id, $message, $type,$data,$title);
            
        } catch (\Exception $e) {
            
        }
    }

    
}
