<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['namespace' => 'Api'],function($request){  
    Route::post('signin', 'UserController@signin'); 
    Route::post('forgot', 'UserController@forgot'); 
    Route::get('get-country', 'UserController@getCountry');
    Route::post('signup', 'UserController@signup'); 
    Route::post('get-static-page', 'UserController@getStaticPage');  
    Route::post('get-app-setting', 'UserController@getAppSetting');  

    // Protected Routes

    Route::group(['middleware' => ['auth:sanctum','sanctum']], function () { 
        Route::post('get-notifications', 'UserController@getNotifications');
        Route::post('get-profile', 'UserController@getProfile'); 
        Route::post('logout', 'UserController@logout'); 
        Route::post('update-profile', 'UserController@updateProfile');
        Route::post('change-password', 'UserController@changePassword');
        Route::post('set-notification-status', 'UserController@setNotificationStatus');  
        Route::post('get-categories', 'UserController@getCategories'); 
        Route::post('get-settings','UserController@getSettings');   
        Route::post('get-nearby-vendors','UserController@getNearbyVendors');   
        Route::post('get-vendor-details','UserController@getVendorDetails');   
        Route::post('get-vendor-giftcards','UserController@getVendorGiftcards');   
        Route::post('get-vendor-reviews','UserController@getVendorReviews');   
        Route::post('add-address','UserController@addAddress');   
        Route::post('get-addresses','UserController@getAddresses');   
        Route::post('get-default-address','UserController@getDefaultAddress');    
        Route::post('set-default-address','UserController@setDefaultAddress');    
        Route::post('delete-address','UserController@deleteAddress');     
        Route::post('redeem-giftcard','UserController@redeemGiftCard');      
        Route::post('my-orders','UserController@myOrders');      
    });
    
});

// Cron and callbacks routes
Route::post('payment-intent-callback', 'CronController@paymentIntentCallback');

// Twilio Room Status Callback
Route::post('room-status-callback', 'CronController@roomStatusCallback');

Route::fallback(function(){
    return response()->json(['status'=>'false','message' => 'Page Not Found'], 404);
});
Route::fallback(function(){
    return response()->json(['status'=>'false','message' => 'Unauthorized','user_status'=>'false'], 401);
});