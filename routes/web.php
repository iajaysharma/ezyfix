<?php

use Illuminate\Support\Facades\Route;
 
Route::get('/',function(){
  return redirect(route('admin.login'));
});
Route::get('admin',function(){
  return redirect(route('admin.login'));
});
Route::get('static/{slug}','Admin\PageController@view')->name('admin.pages.view');
Route::group(['prefix' => 'admin','namespace'=>'Admin','middleware'=>'admin.guest'], function () {
  Route::any('login','AuthController@login')->name('admin.login');  
  Route::post('forgot-password','AuthController@forgotPassword')->name('admin.forgotpassword');
  Route::any('reset-password/{token}','AuthController@resetPassword')->name('admin.resetpassword');
});

Route::group(['prefix' => 'admin','namespace'=>'Admin','middleware'=>'admin'], function () {
  Route::get('home','HomeController@index')->name('admin.home');
  Route::get('toggle-sidebar','HomeController@toggleSidebar')->name('admin.toggle-sidebar');
  Route::any('profile','HomeController@profile')->name('admin.profile');
  Route::any('change-password','HomeController@changePassword')->name('admin.changepassword');
  Route::get('logout','AuthController@logout')->name('admin.logout');

  // Settings Route
  Route::any('settings/add/{id?}','SettingController@add')->name('admin.settings.add');
  Route::any('settings/','SettingController@index')->name('admin.settings.index');
  Route::any('settings/datatables','SettingController@datatables')->name('admin.settings.datatables');

  // Pages Route
  Route::any('pages/add/{id?}','PageController@add')->name('admin.pages.add');
  Route::any('pages/','PageController@index')->name('admin.pages.index');
  Route::any('pages/datatables','PageController@datatables')->name('admin.pages.datatables');
  Route::any('pages/status','PageController@status')->name('admin.pages.status');

    // Categories Route
    Route::any('categories/add/{id?}','CategoryController@add')->name('admin.categories.add');
    Route::any('categories/','CategoryController@index')->name('admin.categories.index');
    Route::any('categories/datatables','CategoryController@datatables')->name('admin.categories.datatables');
    Route::any('categories/status','CategoryController@status')->name('admin.categories.status');
    Route::post('categories/delete/','CategoryController@delete')->name('admin.categories.delete');

    // Vendors Route
    Route::any('vendors/add/{id?}','VendorController@add')->name('admin.vendors.add');
    Route::any('vendors/','VendorController@index')->name('admin.vendors.index');
    Route::any('vendors/datatables','VendorController@datatables')->name('admin.vendors.datatables');
    Route::any('vendors/status','VendorController@status')->name('admin.vendors.status');
    Route::post('vendors/delete/','VendorController@delete')->name('admin.vendors.delete');
    
    // Email Templates Route
    Route::any('emailtemplates/add/{id?}','EmailTemplateController@add')->name('admin.emailtemplates.add');
    Route::any('emailtemplates/','EmailTemplateController@index')->name('admin.emailtemplates.index');
    Route::any('emailtemplates/datatables','EmailTemplateController@datatables')->name('admin.emailtemplates.datatables');
    Route::any('emailtemplates/status','EmailTemplateController@status')->name('admin.emailtemplates.status');

        // Faqs Route
    Route::any('faqs/add/{id?}','FaqController@add')->name('admin.faqs.add');
    Route::any('faqs/','FaqController@index')->name('admin.faqs.index');
    Route::any('faqs/datatables','FaqController@datatables')->name('admin.faqs.datatables');
    Route::any('faqs/status','FaqController@status')->name('admin.faqs.status');
    Route::post('faqs/delete/','FaqController@delete')->name('admin.faqs.delete');

    // Users Route
    Route::get('users','UserController@index')->name('admin.users.index');
    Route::any('users/datatables','UserController@datatables')->name('admin.users.datatables');
    Route::post('users/status','UserController@status')->name('admin.users.status');
    Route::get('users/view/{id}','UserController@view')->name('admin.users.view');
    Route::post('users/delete/','UserController@delete')->name('admin.users.delete');

    Route::any('app-settings','UserController@appSetting')->name('app-settings');

    Route::get('custom-notification','SendNotificationController@index')->name('admin.notification.index');
    Route::any('custom-notification/datatables','SendNotificationController@datatables')->name('admin.notification.datatables');
    Route::any('send-custom-notification','SendNotificationController@send')->name('admin.send.notification');
    Route::post('custom-notification/delete/','SendNotificationController@delete')->name('admin.notification.delete');

});

Route::group(['prefix' => 'vendor','namespace'=>'Vendor','middleware'=>'vendor.guest'], function () {
  Route::any('login','AuthController@login')->name('vendor.login');  
  Route::post('forgot-password','AuthController@forgotPassword')->name('vendor.forgotpassword');
  Route::any('reset-password/{token}','AuthController@resetPassword')->name('vendor.resetpassword');
});


Route::group(['prefix' => 'vendor','namespace'=>'Vendor','middleware'=>'vendor'], function () {
  Route::get('home','HomeController@index')->name('vendor.home');
  Route::get('toggle-sidebar','HomeController@toggleSidebar')->name('vendor.toggle-sidebar');
  Route::any('profile','HomeController@profile')->name('vendor.profile');
  Route::any('change-password','HomeController@changePassword')->name('vendor.changepassword');
  Route::get('logout','AuthController@logout')->name('vendor.logout');


  // Giftcard Routes Route
  Route::any('giftcards/add/{id?}','GiftcardController@add')->name('vendor.giftcards.add');
  Route::any('giftcards/','GiftcardController@index')->name('vendor.giftcards.index');
  Route::any('giftcards/datatables','GiftcardController@datatables')->name('vendor.giftcards.datatables');
  Route::any('giftcards/status','GiftcardController@status')->name('vendor.giftcards.status');
  Route::post('giftcards/delete/','GiftcardController@delete')->name('vendor.giftcards.delete');

  // Gallery Image AuthController
  Route::get('gallery','GalleryController@images')->name('vendor.gallery.images');
  Route::post('gallery/upload-images','GalleryController@uploadImages')->name('vendor.gallery.upload_images');
  Route::get('gallery/get-images','GalleryController@getImages')->name('vendor.gallery.get_images'); 
  Route::post('gallery/delete','GalleryController@deleteImage')->name('vendor.gallery.delete');

  // Orders Image AuthController
  Route::get('orders','OrderController@index')->name('vendor.orders.index'); 
  Route::any('orders/datatables','OrderController@datatables')->name('vendor.orders.datatables');
  Route::any('orders/approval','OrderController@approval')->name('vendor.orders.approval');

});
